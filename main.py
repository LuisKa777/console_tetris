import time
import threading
import defs
rows = 32
columns = 16
matrix = defs.gen_block_matrix(rows, columns)
blocks = []


def moving_matrix(matrix, blocks):
    while (True):
        defs.throw_blocks(matrix, blocks)
        time.sleep(0.3)


thread = threading.Thread(target=moving_matrix, args=(matrix, blocks))
thread.daemon = True
thread.start()

while (True):
    defs.move_tetris(matrix, blocks)
    defs.print_matrix(matrix, blocks)
    time.sleep(0.1)
    defs.clear_lines(matrix)
