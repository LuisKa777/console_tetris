import clases
import random
import keyboard
from colored import fg
CUBE_CHAR = '[]'
WALL_CHAR = '▛▜'
FLOOR_CHAR = '██'
SKY_CHAR = '██'
EMPTY_CHAR = '  '
# coloRs
BOLD = '\033[1m'
NORMAL = '\033[0;0m'


def gen_block_matrix(rows, columns):
    matrix = []
    for row in range(rows):
        r = []
        for column in range(columns):
            if (row == 0):
                r.append(SKY_CHAR)
            elif (row == rows-1):
                r.append(FLOOR_CHAR)
            elif (column == 0 or column == columns-1):
                r.append(WALL_CHAR)
            else:
                r.append(EMPTY_CHAR)
        matrix.append(r)
    return matrix


def clear_lines(matrix):
    rowsNumber = 0
    for i, row in enumerate(matrix):
        if (i > 5):
            rowsNumber+=1
            # print(LINE_UP, end='')
            # print(LINE_CLEAR, end='')
    LINE_UP = '\033['+str(rowsNumber)+'A'
    LINE_CLEAR = '\033[2K'
    print(LINE_UP, end='')
    print(LINE_CLEAR, end='')



def print_matrix(matrix, blocks):
    for i, row in enumerate(matrix):
        r = ""
        for j, column in enumerate(row):
            haveCube = False
            color_cube = NORMAL
            for block in blocks:
                for cube in block.cubes:
                    if (cube[0] == i and cube[1] == j):
                        haveCube = True
                        color_cube = block.color
            r += column if not haveCube else fg(color_cube)+CUBE_CHAR+NORMAL
        if (i > 5):
            print(r)


def block_is_falling(blocks):
    for block in blocks:
        if (block.falling):
            return True
    return False


def get_falling_block(blocks):
    for block in blocks:
        if (block.falling):
            return block
    return False


def create_block(matrix, blocks):
    for i, row in enumerate(matrix):
        start_column = random.randint(3, len(row)-5)
        for j, column in enumerate(row):
            if (i == 2 and j == start_column):
                b = clases.Block(i, j, True, random.randint(1, 7))
                blocks.append(b)
                break


def block_crashed(matrix, blocks, block):
    for cube in block.cubes:
        if (matrix[cube[0]+1][cube[1]] != EMPTY_CHAR):
            return True
        for b in blocks:
            if (not b.falling):
                for c in b.cubes:
                    if (c[0] == cube[0]+1 and c[1] == cube[1]):
                        return True
    return False


def can_move_right(matrix, blocks, block):
    for cube in block.cubes:
        if (matrix[cube[0]][cube[1]+1] != EMPTY_CHAR):
            return False
        for b in blocks:
            if (not b.falling):
                for c in b.cubes:
                    if (c[0] == cube[0] and c[1] == cube[1]+1):
                        return False
    return True


def can_move_left(matrix, blocks, block):
    for cube in block.cubes:
        if (matrix[cube[0]][cube[1]-1] != EMPTY_CHAR):
            return False
        for b in blocks:
            if (not b.falling):
                for c in b.cubes:
                    if (c[0] == cube[0] and c[1] == cube[1]-1):
                        return False
    return True


def can_turn(matrix, blocks, block):
    cbs = block.turn_preview()
    for cube in cbs:
        if (matrix[cube[0]][cube[1]] != EMPTY_CHAR):
            return False
        for b in blocks:
            if (not b.falling):
                for c in b.cubes:
                    if (c[0] == cube[0] and c[1] == cube[1]):
                        return False
    return True


def delete_line(row, cir, blocks):
    for b in blocks:
        for c in cir:
            if c in b.cubes:
                b.cubes.remove(c)
    for b in blocks:
        for c in b.cubes:
            if (c[0] < row and not b.falling):
                c[0] += 1


def delete_completed_lines(matrix, blocks):
    for i, row in enumerate(matrix):
        cubes_in_row = []
        for b in blocks:
            if (not b.falling):
                for c in b.cubes:
                    if (c[0] == i):
                        cubes_in_row.append(c)
        if (len(cubes_in_row) == len(row)-2):
            delete_line(i, cubes_in_row, blocks)


def throw_blocks(matrix, blocks):
    if (block_is_falling(blocks)):
        b = get_falling_block(blocks)
        if (block_crashed(matrix, blocks, b)):
            b.falling = False
        else:
            for cube in b.cubes:
                cube[0] += 1
    else:
        create_block(matrix, blocks)
    delete_completed_lines(matrix, blocks)


def move_tetris(matrix, blocks):
    b = get_falling_block(blocks)
    if (b and block_crashed(matrix, blocks, b)):
        b.falling = False
    else:
        if (b):
            if (keyboard.is_pressed("right") and can_move_right(matrix, blocks, b)):
                for cube in b.cubes:
                    cube[1] += 1
            if (keyboard.is_pressed("left") and can_move_left(matrix, blocks, b)):
                for cube in b.cubes:
                    cube[1] -= 1
            if (keyboard.is_pressed("down")):
                for cube in b.cubes:
                    cube[0] += 1
            if (keyboard.is_pressed("ctrl") and can_turn(matrix, blocks, b)):
                b.turn()
