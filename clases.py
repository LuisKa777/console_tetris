class Block:

    def __init__(self, row, column, falling, form):
        self.row = row
        self.column = column
        self.falling = falling
        self.form = form
        if (form == 1):
            # [][]
            # [][]
            self.cubes = [[row, column], [row+1, column],
                          [row, column+1], [row+1, column+1]]
            self.color = 'orange_1'
        elif (form == 2):
            # [][][][]
            self.cubes = [[row, column], [row, column-1],
                          [row, column+1], [row, column+2]]
            self.color = 'light_cyan'
        elif (form == 3):
            # []
            # [][][]
            self.cubes = [[row, column], [row-1, column-1],
                          [row, column+1], [row, column-1]]
            self.color = 'dodger_blue_2'
        elif (form == 4):
            #     []
            # [][][]
            self.cubes = [[row, column], [row-1, column+1],
                          [row, column+1], [row, column-1]]
            self.color = 'orange_red_1'
        elif (form == 5):
            # [][]
            #   [][]
            self.cubes = [[row, column], [row, column-1],
                          [row+1, column+1], [row+1, column]]
            self.color = 'red_1'
        elif (form == 6):
            #   [][]
            # [][]
            self.cubes = [[row, column], [row, column+1],
                          [row+1, column-1], [row+1, column]]
            self.color = 'green'
        else:
            #   []
            # [][][]
            self.cubes = [[row, column], [row, column-1],
                          [row, column+1], [row-1, column]]
            self.color = 'purple_1a'

    def turn(self):
        if (self.form == 1):
            return
        patron = [[[-1, -1], [-1, 1], [1, 1], [1, -1]],
                  [[0, -2], [-2, 0], [0, 2], [2, 0]],
                  [[-2, -2], [-2, 2], [2, 2], [2, -2]]]
        m = self.cubes[0]
        for c in self.cubes:
            pos = patron[0][0]
            if (c != m):
                if (c[0] == m[0]+1 and c[1] == m[1]):
                    pos = patron[0][0]
                elif (c[0] == m[0] and c[1] == m[1]-1):
                    pos = patron[0][1]
                elif (c[0] == m[0]-1 and c[1] == m[1]):
                    pos = patron[0][2]
                elif (c[0] == m[0] and c[1] == m[1]+1):
                    pos = patron[0][3]
                elif (c[0] > m[0] and c[1] > m[1]):
                    pos = patron[1][0]
                elif (c[0] > m[0] and c[1] < m[1]):
                    pos = patron[1][1]
                elif (c[0] < m[0] and c[1] < m[1]):
                    pos = patron[1][2]
                elif (c[0] < m[0] and c[1] > m[1]):
                    pos = patron[1][3]
                elif (c[0] == m[0]+2 and c[1] == m[1]):
                    pos = patron[2][0]
                elif (c[0] == m[0] and c[1] == m[1]-2):
                    pos = patron[2][1]
                elif (c[0] == m[0]-2 and c[1] == m[1]):
                    pos = patron[2][2]
                elif (c[0] == m[0] and c[1] == m[1]+2):
                    pos = patron[2][3]

                c[0] = c[0]+pos[0]
                c[1] = c[1]+pos[1]

    def turn_preview(self):
        if (self.form == 1):
            return self.cubes
        patron = [[[-1, -1], [-1, 1], [1, 1], [1, -1]],
                  [[0, -2], [-2, 0], [0, 2], [2, 0]],
                  [[-2, -2], [-2, 2], [2, 2], [2, -2]]]
        m = self.cubes[0]
        preview = [m]
        for c in self.cubes:
            pos = patron[0][0]
            temp_c = [c[0], c[1]]
            if (c != m):
                if (c[0] == m[0]+1 and c[1] == m[1]):
                    pos = patron[0][0]
                elif (c[0] == m[0] and c[1] == m[1]-1):
                    pos = patron[0][1]
                elif (c[0] == m[0]-1 and c[1] == m[1]):
                    pos = patron[0][2]
                elif (c[0] == m[0] and c[1] == m[1]+1):
                    pos = patron[0][3]
                elif (c[0] > m[0] and c[1] > m[1]):
                    pos = patron[1][0]
                elif (c[0] > m[0] and c[1] < m[1]):
                    pos = patron[1][1]
                elif (c[0] < m[0] and c[1] < m[1]):
                    pos = patron[1][2]
                elif (c[0] < m[0] and c[1] > m[1]):
                    pos = patron[1][3]
                elif (c[0] == m[0]+2 and c[1] == m[1]):
                    pos = patron[2][0]
                elif (c[0] == m[0] and c[1] == m[1]-2):
                    pos = patron[2][1]
                elif (c[0] == m[0]-2 and c[1] == m[1]):
                    pos = patron[2][2]
                elif (c[0] == m[0] and c[1] == m[1]+2):
                    pos = patron[2][3]
                temp_c[0] = c[0]+pos[0]
                temp_c[1] = c[1]+pos[1]
                preview.append(temp_c)
        return preview
